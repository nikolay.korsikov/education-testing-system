import re
import csv
import os
import hashlib
import os.path
pass_regEx = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$'
regex_name = re.compile("^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)")
regex_login = r'^(?![-._])(?!.*[_.-]{2})[\w.-]{6,30}(?<![-._])$'
user_fieldnames = ['full_name', 'address', 'phone', 'login', 'password']

def create_userfile():
    with open('user_pass.csv', 'w+', encoding='UTF8', newline='') as f:
        global user_fieldnames
        writer = csv.DictWriter(f, fieldnames=user_fieldnames)
        writer.writeheader()
        f.close()

def login_encrypt(input_login):
    encoded_login = hashlib.md5(input_login.encode("utf-8")).hexdigest()
    return encoded_login

def pass_encrypt(input_pass):
    encoded_pass = hashlib.md5(input_pass.encode("utf-8")).hexdigest()
    return encoded_pass

def adminRegistration():
    admin_info = {}
    while True:
        try:
            while True:
                print("Note that the username may only contain:\n"
                        "Uppercase and lowercase letters;\n"
                        "Numbers from 0-9; and\n"
                        "Special characters '_' '-' '.'\n"
                        "\n"
                        "Username may not:\n"
                        "Begin or finish with characters _ - .\n"
                        "Have more than one sequential character '_' '-' '.' inside\n")
                admin_login = input("Enter a Username: ")

                if not re.fullmatch(regex_login, admin_login):
                    print("Invalid Username\n"
                          "Note that the username may only contain:\n"
                          "Uppercase and lowercase letters;\n"
                          "Numbers from 0-9; and\n"
                          "Special characters '_' '-' '.'\n"
                          "\n"
                          "Username may not:\n"
                          "Begin or finish with characters _ - .\n"
                          "Have more than one sequential character '_' '-' '.' inside")
                else:
                    admin_info['login'] = login_encrypt(admin_login)
                    break

            while True:
                print("")
                print('Note that the password must contain:\n'
                      'Minimum eight characters;\n'
                      'At least one uppercase letter;\n'
                      'One lowercase letter;\n'
                      'One number; and\n'
                      'One special character\n')
                admin_password = input("Password: ")

                if not re.fullmatch(pass_regEx, admin_password):
                    print(
                        "The password should contain: \n"
                        "minimum eight characters; \n"
                        "at least one uppercase letter; \n"
                        "one lowercase letter; \n"
                        "one number; and \n"
                        "one special character")
                else:
                    admin_info['password'] = pass_encrypt(admin_password)
                    print("Admin has been registered")
                    break

            with open("admin_pass.csv", 'w+', newline='') as f:
                admin_fieldnames = ['login', 'password']
                writer = csv.DictWriter(f, fieldnames=admin_fieldnames)
                writer.writeheader()
                writer.writerow(admin_info)
                f.close()

        except ValueError as e:
            print("Error: " + str(e) + '\nRegistrations failed. Incorrect Username or Password has been entered.\n')
        except BaseException as e:
            print("Error: " + str(e))
        return admin_info

def user_registration():
    student = {}
    users = {}
    result = 0
    global user_fieldnames
    while True:
        try:
                while True:
                    full_name = input("Please enter your full name: ")
                    if not re.fullmatch(regex_name, full_name):
                        print("Invalid name! Please enter correct name and surname")
                    else:
                        student["full_name"] = full_name
                        break

                while True:
                    students_address = input("Please enter your address: ")
                    if not type(students_address) == str:
                        print("Invalid address! Please enter your full address")
                    else:
                        student["address"] = students_address
                        break

                while True:
                    students_phone = input("Please enter your phone number without a country code: ")
                    if not students_phone.isdigit():
                        print("Invalid phone number! Please enter correct phone number")
                    else:
                        student["phone"] = students_phone
                        break

                while True:
                    students_login = input("Please enter a Username: ")

                    with open("user_pass.csv",'r', newline='') as f:
                        csv_reader = csv.DictReader(f)
                        for row in csv_reader:
                            username = row['login']
                            password = row['password']
                            users[username] = password

                    if not re.fullmatch(regex_login, students_login):
                        print("Invalid Username\n"
                              "Note that the username may only contain:\n"
                              "Uppercase and lowercase letters;\n"
                              "Numbers from 0-9; and\n"
                              "Special characters '_' '-' '.'\n"
                              "\n"
                              "Username may not:\n"
                              "Begin or finish with characters _ - .\n"
                              "Have more than one sequential character '_' '-' '.' inside")
                    else:
                        if len(users) == 0:
                            student["login"] = login_encrypt(students_login)
                            result = 1
                        elif not login_encrypt(students_login) in users.keys():
                            student["login"] = login_encrypt(students_login)
                            result = -1
                        elif login_encrypt(students_login) in users.keys():
                            result = 2

                        if result == 1:
                            break
                        if result == -1:
                            break
                        if result == 2:
                            print(f"User name: '{students_login}' already exists, please enter another Username")
                            continue

                while True:
                    students_password = input("Please enter your password.\n"
                                              "Note: the password must contain minimum of eight characters; \n"
                                              "at least one uppercase letter; \n"
                                              "one lowercase letter; \n"
                                              "one number and one special character: ")
                    if not re.fullmatch(pass_regEx, students_password):
                        print("The password should contain minimum eight characters, \n"
                              "at least one uppercase letter, \n"
                              "one lowercase letter, \n"
                              "one number and one special character")
                    else:
                        student["password"] = login_encrypt(students_password)
                        break

                with open("user_pass.csv",'a+', newline='') as f:
                    global user_fieldnames
                    writer = csv.DictWriter(f, fieldnames=user_fieldnames)
                    writer.writerow(student)
                    f.close()
                    print()
                    print("User has been registered")
        except ValueError as e:
            print("Error: " + str(e) + "\nPlease check your inputs\n")
        except BaseException as e:
            print("Error: " + str(e))
        break

def user_validation(check_username, check_password):
    users = {}
    while True:
        with open("admin_pass.csv", 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                admin_username = row['login']
                admin_password = row['password']
            f.close()
        with open('user_pass.csv','r', newline='') as d:
            csv_reader = csv.DictReader(d)
            for row in csv_reader:
                username = row['login']
                password = row['password']
                users[username] = password
            d.close()

        if (login_encrypt(check_username), pass_encrypt(check_password)) in users.items():
            result = 1

        elif admin_username == login_encrypt(check_username) and admin_password == pass_encrypt(check_password):
            result = 2
        else:
            result = -1

        if result == 1:
            break

        elif result == 2:
            break

        elif result == -1:
            break
    return result


def user_menu():
    while True:
        if os.stat('admin_pass.csv').st_size == 0:
            print("Hello this the first launch of the Education Testing System")
            print(" *** Admin Sign Up *** ")
            print("Please enter Username and Password for registration")
            print()
            adminRegistration()
            continue
        else:
            print()
            print("Hello this is the Education Testing System")
            print("Please select an option:\n 1. Sign in\n 2. Register account\n 3. Exit")
            user_answer = int(input("Option: "))
            if user_answer == 1 or 2 or 3:
                return user_answer
            else:
                print("Enter a valid option")

create_userfile()

while True:
    try:
        answer = user_menu()
        if answer == 1:
            print("Please enter Username and Password to sign in: ")
            check_username = input("Please enter Username: ")
            check_password = input("Please enter user password: ")
            user_validation(check_username, check_password)
            if user_validation(check_username, check_password) == 1:
                print("Welcome Student")
                continue

            elif user_validation(check_username, check_password) == 2:
                print("Welcome Admin")
                continue

            elif user_validation(check_username, check_password) == -1:
                print("incorrect username or password entered. Please try again: ")
                continue
        elif answer == 2:
            print("Please provide students details: ")
            user_registration()
            continue
        elif answer == 3:
            print("Thank you and see you soon")
            break
    except ValueError as e:
        print("Error: " + str(e) + "\nPlease check your inputs\n")
    except BaseException as e:
        print("Error: " + str(e))
        user_menu()


