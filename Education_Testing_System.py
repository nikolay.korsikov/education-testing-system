import re
import csv
import os
import hashlib
import os.path
pass_regEx = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$'
regex_name = re.compile("^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)")
regex_login = r'^(?![-._])(?!.*[_.-]{2})[\w.-]{6,30}(?<![-._])$'
phone_pattern = r'(?:\+\d{2})?\d{3,4}\D?\d{3}\D?\d{3}'
user_fieldnames = ['full_name', 'address', 'phone', 'login', 'password']
file_name = 'user_pass.csv'
file_name2 = 'tests_base.csv'
file_name3 = 'tests_stat.csv'
answer_t = 0
answer_adm = 0

def create_userfile():
    with open('user_pass.csv', 'w+', encoding='UTF8', newline='') as f:
        global user_fieldnames
        writer = csv.DictWriter(f, fieldnames=user_fieldnames)
        writer.writeheader()
        f.close()

def login_encrypt(input_login):
    encoded_login = hashlib.md5(input_login.encode("utf-8")).hexdigest()
    return encoded_login

def pass_encrypt(input_pass):
    encoded_pass = hashlib.md5(input_pass.encode("utf-8")).hexdigest()
    return encoded_pass

def adminRegistration():
    admin_info = {}
    while True:
        try:
            while True:
                print("Note that the username may only contain:\n"
                        "Uppercase and lowercase letters;\n"
                        "Numbers from 0-9; and\n"
                        "Special characters '_' '-' '.'\n"
                        "\n"
                        "Username may not:\n"
                        "Begin or finish with characters _ - .\n"
                        "Have more than one sequential character '_' '-' '.' inside\n")
                admin_login = input("Enter a Username: ")

                if not re.fullmatch(regex_login, admin_login):
                    print("Invalid Username\n"
                          "Note that the username may only contain:\n"
                          "Uppercase and lowercase letters;\n"
                          "Numbers from 0-9; and\n"
                          "Special characters '_' '-' '.'\n"
                          "\n"
                          "Username may not:\n"
                          "Begin or finish with characters _ - .\n"
                          "Have more than one sequential character '_' '-' '.' inside")
                else:
                    admin_info['login'] = login_encrypt(admin_login)
                    break

            while True:
                print("")
                print('Note that the password must contain:\n'
                      'Minimum eight characters;\n'
                      'At least one uppercase letter;\n'
                      'One lowercase letter;\n'
                      'One number; and\n'
                      'One special character\n')
                admin_password = input("Password: ")

                if not re.fullmatch(pass_regEx, admin_password):
                    print(
                        "The password should contain: \n"
                        "minimum eight characters; \n"
                        "at least one uppercase letter; \n"
                        "one lowercase letter; \n"
                        "one number; and \n"
                        "one special character")
                else:
                    admin_info['password'] = pass_encrypt(admin_password)
                    print("Admin has been registered")
                    break

            with open("admin_pass.csv", 'w+', newline='') as f:
                admin_fieldnames = ['login', 'password']
                writer = csv.DictWriter(f, fieldnames=admin_fieldnames)
                writer.writeheader()
                writer.writerow(admin_info)
                f.close()

        except ValueError as e:
            print("Error: " + str(e) + '\nRegistrations failed. Incorrect Username or Password has been entered.\n')
        except BaseException as e:
            print("Error: " + str(e))
        return admin_info

def user_registration():
    student = {}
    users = {}
    result = 0
    global user_fieldnames
    while True:
        try:
            while True:
                full_name = input("Please enter your full name: ")
                if not re.fullmatch(regex_name, full_name):
                    print("Invalid name! Please enter correct name and surname")
                else:
                    student["full_name"] = full_name
                    break

            while True:
                students_address = input("Please enter your address: ")
                if not type(students_address) == str:
                    print("Invalid address! Please enter your full address")
                else:
                    student["address"] = students_address
                    break

            while True:
                students_phone = input("Please enter your phone number: ")
                if not re.fullmatch(phone_pattern, students_phone):
                    print("Invalid phone number! Please enter correct phone number")
                else:
                    student["phone"] = students_phone
                    break

            while True:
                students_login = input("Please enter a Username: ")

                with open("user_pass.csv", 'r', newline='') as f:
                    csv_reader = csv.DictReader(f)
                    for row in csv_reader:
                        username = row['login']
                        password = row['password']
                        users[username] = password


                if not re.fullmatch(regex_login, students_login):
                    print("Invalid Username\n"
                          "Note that the username may only contain:\n"
                          "Uppercase and lowercase letters;\n"
                          "Numbers from 0-9; and\n"
                          "Special characters '_' '-' '.'\n"
                          "\n"
                          "Username may not:\n"
                          "Begin or finish with characters _ - .\n"
                          "Have more than one sequential character '_' '-' '.' inside")
                else:
                    if len(users) == 0:
                        student["login"] = login_encrypt(students_login)
                        result = 1
                    elif not login_encrypt(students_login) in users.keys():
                        student["login"] = login_encrypt(students_login)
                        result = -1
                    elif login_encrypt(students_login) in users.keys():
                        result = 2

                    if result == 1:
                        break

                while True:
                    students_address = input("Please enter your address: ")
                    if not type(students_address) == str:
                        print("Invalid address! Please enter your full address")
                    else:
                        student["address"] = students_address
                        break

                while True:
                    students_phone = input("Please enter your phone number without a country code: ")
                    if not students_phone.isdigit():
                        print("Invalid phone number! Please enter correct phone number")
                    else:
                        student["phone"] = students_phone
                        break

            with open("user_pass.csv", 'a+', newline='') as f:
                global user_fieldnames
                writer = csv.DictWriter(f, fieldnames=user_fieldnames)
                writer.writerow(student)
                f.close()
                print()
                print("User has been registered")
        except ValueError as e:
            print("Error: " + str(e) + "\nPlease check your inputs\n")
        except BaseException as e:
            print("Error: " + str(e))
        break

def user_validation(check_username, check_password):
    users = {}
    while True:
        with open("admin_pass.csv", 'r') as f:
            reader = csv.DictReader(f)
            for row in reader:
                admin_username = row['login']
                admin_password = row['password']
            f.close()
        with open('user_pass.csv','r', newline='') as d:
            csv_reader = csv.DictReader(d)
            for row in csv_reader:
                username = row['login']
                password = row['password']
                users[username] = password
            d.close()

        if (login_encrypt(check_username), pass_encrypt(check_password)) in users.items():
            result = 1

        elif admin_username == login_encrypt(check_username) and admin_password == pass_encrypt(check_password):
            result = 2
        else:
            result = -1

        if result == 1:
            break

        elif result == 2:
            break

        elif result == -1:
            break
    return result


def menu():
    print("\nHello admin! \n1. Change your login and password\n2. Student profile administration \n3. Review test results \n4. Tests administration"
          "\n5. Exit ")
    while True:
        try:
            return int(input("Option: "))
        except ValueError as e:
            print("Error: " + str(e) + "\nPlease check your inputs\n")
        except BaseException as e:
            print("Error: " + str(e))


def menu_user_settings():       #управление пользователем
    print("\n1. Add account for a new user\n2. Remove user \n3. Change user \n4. Exit")
    while True:
        try:
            return int(input("Option: "))
        except ValueError as e:
            print("Error: " + str(e) + "\nPlease check your inputs\n")
        except BaseException as e:
            print("Error: " + str(e))


def add_profile():                   #Добавление пользователя
    global user_fieldnames
    profile = {}
    while True:
        full_name = input("\nUser full name  ")
        if len(full_name) < 50:
            match = re.fullmatch(regex_name, full_name)
            if match:
                profile['full_name'] = full_name
                break
            else:
                print("Error: " + full_name + "\nPlease check your inputs\n")
                continue
    while True:
        address = input("User address:  ")
        if type(address)== str and len(address) < 100:
            profile['address'] = address
            break
        else:
            print("Error: " + address + "\nPlease check your inputs\n")
            continue
    while True:
        phone = input("User phone:  ")
        if 16 > len(phone) > 9:
            match = re.fullmatch(phone_pattern, phone)
            if match:
                profile["phone"] = phone
                break
            else:
                print("Error: " + phone + "\nPlease check your inputs\n")
                continue
    while True:
        login = input("User login:  ")
        if len(login) <= 10:
            match = re.fullmatch(regex_login, login)
            if match:
                profile["login"] = login_encrypt(login)
                break
            else:
                print("Error: " + login + "\nPlease check your inputs\n")
                continue
    while True:
        password = input("User temporary password: ")
        if len(password) < 15:
            match = re.fullmatch(pass_regEx, password)
            if match:
                profile["password"] = pass_encrypt(password)
                break
            else:
                print("Error: " + password + "\nPlease check your inputs\n")
                continue
    return profile


def search_profile():    #поиск пользователя по фамилии или логину
    global search
    profile_found = []
    fields = {1: 'full_name', 2: 'login'}
    print("\nChoose your option:\n"
          "1. Full Name\n2. Login\n3. Exit")
    while True:
        try:
            answers = int(input("Option: "))
            while answers != 3:
                if answers == 2:
                    search = input("Login: ")
                elif answers == 1:
                    search = input("Full Name: ")
                if answers in fields:
                    selected = fields[answers]
                    with open(file_name, 'r', encoding='utf-8') as f:
                        reader = csv.DictReader(f)
                        for i in reader:
                            if selected == 'full_name':
                                if i[selected] == search:
                                    profile_found.append(i)
                            else:
                                # selected == 'login'
                                if i[selected] == login_encrypt(search):
                                    profile_found.append(i)

                break
        except ValueError as e:
            print("Error: " + str(e) + "\nPlease check your inputs\n")
        except BaseException as e:
            print("Error: " + str(e))
        return profile_found



def writer_function(x):
    user_fieldnames = ['full_name', 'address', 'phone', 'login', 'password']
    with open(file_name, 'w', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=user_fieldnames)
        writer.writeheader()
        writer.writerows(x)
        f.close()


def remove_profile():      #удаление данных пользователя
    ml=[]
    found = 0
    to_remove_profile = search_profile()
    if len(to_remove_profile) != 0:
        with open(file_name, 'r', encoding='utf-8') as f:
            reader = csv.DictReader(f)
            for i in reader:
                for j in to_remove_profile:
                    if i != j:
                        ml.append(i)
                    else:
                        found=1
            f.close()
    if found == 0:
        print('User is not in the database')
    else:
        writer_function(ml)
        print('Record was deleted successfully')


def profile_change():      #изменение данных пользователя
    new_profile=[]
    found = 0
    new_answer = 0
    to_change_profile = search_profile()
    if len(to_change_profile) != 0:
        with open(file_name, 'r', encoding='utf-8') as f:
            reader = csv.DictReader(f)
            for i in reader:
                for j in to_change_profile:
                    if i != j:
                        new_profile.append(i)
                    else:
                        found = 1
            f.close()
    if found == 1:
        for i in to_change_profile:
            ind = to_change_profile.index(i)
            print(f'What would you like to change?')
            print("Options:\n"
                  "1. Full Name.\n2. Address\n3. Phone\n4. Login\n5. Password\n6. Exit")
            for _ in i.items():
                while new_answer != 6:
                        try:
                            new_answer = int(input("Option: "))
                            while new_answer != 6:
                                if new_answer == 1:
                                    to_change_profile[ind]['full_name'] = input('Full Name: ')
                                elif new_answer == 2:
                                    to_change_profile[ind]['address'] = input('Address: ')
                                elif new_answer == 3:
                                    to_change_profile[ind]['phone'] = input("Phone: ")
                                elif new_answer == 4:
                                    to_change_profile[ind]['login'] = login_encrypt(input("Login: "))
                                elif new_answer == 5:
                                    to_change_profile[ind]['password'] = pass_encrypt(input("Password: "))
                                break
                            break
                        except ValueError as e:
                            print("Error: " + str(e) + "\nEnter a valid option\n")
                        except BaseException as e:
                            print("Error: " + str(e))
        writer_function(new_profile)
    else:
        i = 0
    return i


def tests_administration():       #администрирование тестов
    print("\n1. Add new tests (subject, category, tests)\n2. Remove tests \n3. Change tests /answers \n4. Exit")
    while True:
        try:
            return int(input("Option: "))
        except ValueError as e:
            print("Error: " + str(e) + "\nPlease check your inputs\n")
        except BaseException as e:
            print("Error: " + str(e))


def add_tests():           #Добавление тестов (предмет, категория, тесты, варианты ответов, правильный ответ)
    global tests_fieldnames
    new_test = {}
    while True:
        subject = input("\nSubject (e.g.Physics) ")
        if len(subject) < 40:
            new_test['Subject'] = subject
            break
        else:
            print("Error: " + subject + "\nPlease check your inputs\n")
            continue
    while True:
        category = input("Category (e.g.Mechanics)  ")
        if len(category) < 50:
            new_test['Category'] = category
            break
        else:
            print("Error: " + category + "\nPlease check your inputs\n")
            continue
    while True:
        testid = input("Test ID (e.g.PM-123):  ")
        if len(testid) == 6:
            new_test["Test ID"] = testid
            break
        else:
            print("Error: " + testid + "\nPlease check your inputs\n")
            continue
    while True:
        test_desc = input("Test description:  ")
        if len(test_desc) < 150:
            new_test["Test descr"] = test_desc
            break
        else:
            print("Error: " + test_desc + "\nPlease check your inputs\n")
            continue
    while True:
        test_option1 = input("Test option #1:  ")
        if len(test_option1) < 25:
            new_test["Option1"] = test_option1
            break
        else:
            print("Error: " + test_option1 + "\nPlease check your inputs\n")
            continue
    while True:
        test_option2 = input("Test option #2:  ")
        if len(test_option2) < 25:
            new_test["Option1"] = test_option2
            break
        else:
            print("Error: " + test_option2 + "\nPlease check your inputs\n")
            continue
    while True:
        test_option3 = input("Test option #3:  ")
        if len(test_option3) < 25:
            new_test["Option1"] = test_option3
            break
        else:
            print("Error: " + test_option3 + "\nPlease check your inputs\n")
            continue
    while True:
        test_option4 = input("Test option #4:  ")
        if len(test_option4) < 25:
            new_test["Option4"] = test_option4
            break
        else:
            print("Error: " + test_option4 + "\nPlease check your inputs\n")
            continue
    while True:
        test_option5 = input("Test option #5:  ")
        if len(test_option5) < 25:
            new_test["Option5"] = test_option5
            break
        else:
            print("Error: " + test_option5 + "\nPlease check your inputs\n")
            continue
    while True:
        test_answer = int(input("Answer(Option number), e.g.4 :  "))
        if test_answer <= 5:
            new_test["test_answer"] = test_answer
            break
    return new_test


def search_tests():    #поиск теста по предмету, категории и тест ID
    global search_t
    tests_found = []
    fields = {1: 'Subject', 2: 'Category', 3: 'Test ID'}
    print("\nChoose your option:\n"
          "1. Subject\n2. Category\n3. Test ID \n4. Exit")
    while True:
        try:
            answers = int(input("Option: "))
            while answers != 4:
                if answers == 1:
                    search_t = input("Subject: ")
                elif answers == 2:
                    search_t = input("Category: ")
                elif answers == 3:
                    search_t = input("Test ID: ")
                if answers in fields:
                    selected_t = fields[answers]
                    with open(file_name2, 'r', encoding='utf-8') as f:
                        reader = csv.DictReader(f)
                        for i in reader:
                            if selected_t == 'Subject':
                                if i[selected_t] == search_t:
                                    tests_found.append(i)
                            elif selected_t == 'Category':
                                if i[selected_t] == search_t:
                                    tests_found.append(i)
                            elif selected_t == 'Test ID':
                                if i[selected_t] == search_t:
                                    tests_found.append(i)
                break
        except ValueError as e:
            print("Error: " + str(e) + "\nPlease check your inputs\n")
        except BaseException as e:
            print("Error: " + str(e))
        return tests_found



def writer_tests(x):
    tests_fieldnames = ['Subject','Category','Test ID','Test descr','Option1','Option2','Option3','Option4',
                        'Option5','test_answer']
    with open(file_name2, 'w', newline='',encoding='utf-8') as f:
        writer = csv.DictWriter(f, fieldnames=tests_fieldnames)
        writer.writeheader()
        writer.writerows(x)
        f.close()


def remove_tests():      #удаление тестов
    ml=[]
    found = 0
    to_remove_test = search_tests()
    if len(to_remove_test) != 0:
        with open(file_name2, 'r', encoding='utf-8') as f:
            reader = csv.DictReader(f)
            for i in reader:
                for j in to_remove_test:
                    if i != j:
                        ml.append(i)
                    else:
                        found=1
            f.close()
    if found == 0:
        print('Test is not in the database')
    else:
        writer_tests(ml)
        print('Record was deleted successfully')


def tests_change():      #изменение тестов (предмет, категория, тест, ответы)
    updated_test=[]
    found = 0
    new_answer = 0
    to_change_test = search_tests()
    if len(to_change_test) != 0:
        with open(file_name2, 'r', encoding='utf-8') as f:
            reader = csv.DictReader(f)
            for i in reader:
                for j in to_change_test:
                    if i != j:
                        updated_test.append(i)
                    else:
                        found = 1
            f.close()
    if found == 1:
        for i in to_change_test:
            ind = to_change_test.index(i)
            print(f'What would you like to change in the test: {i}?')
            print("Options:\n"
                  "1. Subject.\n2. Category\n3. Test descr\n4. Option1\n5. Option2\n6. Option3 \n7. Option4\n8. Option5"
                  "\n9. Answer number \n10. Exit")
            for _ in i.items():
                while new_answer != 10:
                        try:
                            new_answer = int(input("Option: "))
                            while new_answer != 10:
                                if new_answer == 1:
                                    to_change_test[ind]['Subject'] = input('Subject: ')
                                elif new_answer == 2:
                                    to_change_test[ind]['Category'] = input('Category: ')
                                elif new_answer == 3:
                                    to_change_test[ind]['Test descr'] = input("Test description: ")
                                elif new_answer == 4:
                                    to_change_test[ind]['Option1'] = input("Test option#1: ")
                                elif new_answer == 5:
                                    to_change_test[ind]['Option2'] = input("Test option#2: ")
                                elif new_answer == 6:
                                    to_change_test[ind]['Option3'] = input("Test option#3: ")
                                elif new_answer == 7:
                                    to_change_test[ind]['Option4'] = input("Test option#4: ")
                                elif new_answer == 8:
                                    to_change_test[ind]['Option5'] = input("Test option#5: ")
                                elif new_answer == 9:
                                    to_change_test[ind]['test_answer'] =int(input("Answer(Option number), e.g.4 : "))
                                break
                            break
                        except ValueError as e:
                            print("Error: " + str(e) + "\nEnter a valid option\n")
                        except BaseException as e:
                            print("Error: " + str(e))
        writer_tests(updated_test)
    else:
        i = 0
    return i


def admin_log_pw_search():     #администрирование логина и пароля самого админа
    global search_adm, answers_adm
    info_found = []
    fields = {1: 'login', 2: 'password'}
    print ('What would you like to change?')
    print("\n1. Login\n2. Password \n3. Exit")
    while True:
        try:
            answers_adm = int(input("Option: "))
            while answers_adm != 3:
                if answers_adm == 1:
                    search_adm = input("Current login: ")
                elif answers_adm == 2:
                    search_adm = input("Current password: ")
                if answers_adm in fields:
                    selected_adm = fields[answers_adm]
                    with open('admin_pass.csv', 'r', encoding='utf-8') as f:
                        reader = csv.DictReader(f)
                        for i in reader:
                            if selected_adm == 'login':
                                if i[selected_adm] == login_encrypt(search_adm):
                                    info_found.append(i)
                            elif selected_adm == 'password':
                                if i[selected_adm] == pass_encrypt(search_adm):
                                    info_found.append(i)
                break
        except ValueError as e:
            print("Error: " + str(e) + "\nPlease check your inputs\n")
        except BaseException as e:
            print("Error: " + str(e))
        return info_found

def admin_log_pw_change():
    adm_fieldnames = ['login', 'password']
    updated_adm = []
    found = 0
    global answers_adm
    adm_info =admin_log_pw_search()
    if len(adm_info) != 0:
        with open('admin_pass.csv', 'r', encoding='utf-8') as f:
            reader = csv.DictReader(f)
            for i in reader:
                for j in adm_info:
                    if i != j:
                        updated_adm.append(i)
                    else:
                        found = 1
            f.close()
    if found == 1:
        for i in adm_info:
            ind = adm_info.index(i)
            for _ in i.items():
                if answers_adm == 1:
                    adm_info[ind]['login'] = login_encrypt(input('New login: '))
                    print('Your login was changed successfully. Would you like to change your password?')
                    print('If yes, just come back and choose option #2')
                elif answers_adm == 2:
                 adm_info[ind]['password'] = pass_encrypt(input('New Password: '))
                break
            break
        with open('admin_pass.csv', 'w', newline='', encoding='utf-8') as f:
            writer = csv.DictWriter(f, fieldnames=adm_fieldnames)
            writer.writeheader()
            writer.writerows(updated_adm)
            f.close()
    else:
        i = 0
    return i


def tests_review():  # поиск статистики по тесту
    global search_stat
    tests_stat = []
    fields = {1: 'login', 2: 'Subject', 3: 'Category', 4: 'Test ID'}
    print("\nChoose your option:\n"
            "1. User_login\n2. Subject\n3. Category \n4. Test ID \n5. Exit")
    while True:
        try:
            answers = int(input("Option: "))
            while answers != 5:
                if answers == 1:
                    search_stat = login_encrypt(input("User_login: "))
                elif answers == 2:
                    search_stat = input("Subject: ")
                elif answers == 3:
                    search_stat = input("Category: ")
                elif answers == 4:
                   search_stat = input("Test ID: ")
                if answers in fields:
                    selected_st = fields[answers]
                    with open(file_name3, 'r', encoding='utf-8') as f:
                        reader = csv.DictReader(f)
                        for i in reader:
                            if selected_st == 'Subject':
                                if i[selected_st] == search_stat:
                                    tests_stat.append(i)
                            elif selected_st == 'Category':
                                if i[selected_st] == search_stat:
                                    tests_stat.append(i)
                            elif selected_st == 'Test ID':
                                if i[selected_st] == search_stat:
                                    tests_stat.append(i)
                            elif selected_st == 'login':
                                if i[selected_st] == search_stat:
                                    tests_stat.append(i)

                break
        except ValueError as e:
            print("Error: " + str(e) + "\nPlease check your inputs\n")
        except BaseException as e:
            print("Error: " + str(e))
        return tests_stat

def view_tests(stat):
        for key, val in stat.items():
            print(f"{key} - {val}")

def user_menu():
    while True:
        if os.stat('admin_pass.csv').st_size == 0:
            print("Hello this the first launch of the Education Testing System")
            print(" *** Admin Sign Up *** ")
            print("Please enter Username and Password for registration")
            print()
            adminRegistration()
            continue
        else:
            print()
            print("Hello this is the Education Testing System")
            print("Please select an option:\n 1. Sign in\n 2. Register account\n 3. Exit")
            user_answer = int(input("Option: "))
            if user_answer == 1 or 2 or 3:
                return user_answer
            else:
                print("Enter a valid option")
create_userfile()

while True:
    try:
        answer = user_menu()
        if answer == 1:
            print("Please enter Username and Password to sign in: ")
            check_username = input("Please enter Username: ")
            check_password = input("Please enter user password: ")
            user_validation(check_username, check_password)
            if user_validation(check_username, check_password) == 1:
                print("Welcome Student")
                continue

            elif user_validation(check_username, check_password) == 2:
                while answer_adm != 5:
                    answer_adm = menu()
                    if answer_adm == 1:
                        adm = admin_log_pw_change()    # изменить логин и пароль админа
                        if adm != 0:
                            from csv import DictWriter
                            adm_fieldnames = ['login', 'password']
                            with open('admin_pass.csv', 'a', newline='', encoding='utf-8') as f:
                                dictwriter_object = DictWriter(f, fieldnames=adm_fieldnames)
                                dictwriter_object.writerow(adm)
                                f.close()
                        else:
                            print('Login or password are not correct')
                    elif answer_adm == 2:
                        while answer != 4:  # управление пользователем
                            answer = menu_user_settings()
                            if answer == 1:
                                from csv import DictWriter
                                user_fieldnames = ['full_name', 'address', 'phone', 'login', 'password']
                                with open(file_name, 'a', newline='') as f:
                                    dictwriter_object = DictWriter(f, fieldnames=user_fieldnames)
                                    dictwriter_object.writerow(add_profile())
                                    f.close()
                            elif answer == 2:
                                remove_profile()
                            elif answer == 3:
                                new_prof = profile_change()
                                if new_prof != 0:
                                    from csv import DictWriter
                                    user_fieldnames = ['full_name', 'address', 'phone', 'login', 'password']
                                    with open(file_name, 'a', newline='') as f:
                                        dictwriter_object = DictWriter(f, fieldnames=user_fieldnames)
                                        dictwriter_object.writerow(new_prof)
                                        f.close()
                                else:
                                    print('User is not in database')
                        continue
                    elif answer_adm == 3:   # просмотр результатов тестирования
                        ts = tests_review()
                        if len(ts) != 0:
                            for i in ts:
                                view_tests(i)
                            print('Would you like to save it separately?')
                            flag = input('Please enter y or n   ')
                            if flag == 'y':
                                new_file_name = input('Write the name of csv-file (e.g.firmB.csv)   ')
                                with open(new_file_name, 'w', newline='') as f:
                                    fieldnames_new = ['login', 'Subject', 'Category','Test ID', 'Test descr',
                                                      'stud_answer', 'rate']
                                    writer = csv.DictWriter(f, fieldnames=fieldnames_new)
                                    writer.writeheader()
                                    writer.writerows(ts)
                                    f.close()
                    elif answer_adm == 4:
                        while answer_t != 4:  # администрирование тестов
                            answer_t = tests_administration()
                            if answer_t == 1:
                                from csv import DictWriter
                                tests_fieldnames = ['Subject','Category','Test ID','Test descr','Option1','Option2',
                                                    'Option3','Option4','Option5','test_answer']
                                with open(file_name2, 'a', newline='',encoding='utf-8') as f:
                                    dictwriter_object = DictWriter(f, fieldnames=tests_fieldnames)
                                    dictwriter_object.writerow(add_tests())
                                    f.close()
                            elif answer_t == 2:
                                remove_tests()
                            elif answer_t == 3:
                                updated_test = tests_change()
                                if updated_test != 0:
                                    from csv import DictWriter
                                    tests_fieldnames = ['Subject','Category','Test ID','Test descr','Option1','Option2',
                                                    'Option3','Option4','Option5','test_answer']
                                    with open(file_name2, 'a', newline='',encoding='utf-8') as f:
                                        dictwriter_object = DictWriter(f, fieldnames=tests_fieldnames)
                                        dictwriter_object.writerow(updated_test)
                                        f.close()
                                else:
                                    print('Test is not in database')
                        continue
        elif answer == 3:
            print("Thank you and see you soon")
            break
    except ValueError as e:
        print("Error: " + str(e) + "\nPlease check your inputs\n")
    except BaseException as e:
        print("Error: " + str(e))



